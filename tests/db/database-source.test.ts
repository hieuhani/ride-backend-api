import sinon from "sinon";
import { Database } from "sqlite3";
import { expect } from "chai";

import { Ride } from "../../src/ride/models";
import { DatabaseSource } from "../../src/db";

describe("DatabaseSource", () => {
  describe("constructor", () => {
    it("should be initialized", (done) => {
      const db = new Database(":memory:");
      const dataSource = new DatabaseSource<Ride>(db, "Rides");
      expect(dataSource).not.to.undefined;
      done();
    });
  });

  describe("#find", () => {
    describe("find success", () => {
      it("shoud invoke db all function to return data", async () => {
        const fakeResponse = {
          rideID: 1,
          startLat: 2,
          startLong: 3,
          endLat: 4,
          endLong: 9,
          riderName: "Hieu",
          driverName: "Hieu",
          driverVehicle: "Honda SH",
          created: "2021-08-21 11:36:54",
        };
        const db: any = {
          all: sinon.stub().callsArgWith(2, null, [fakeResponse]),
        };
        const dataSource = new DatabaseSource<Ride>(db, "Rides");
        expect(dataSource).not.to.undefined;

        const response = await dataSource.find();
        Object.keys(fakeResponse).forEach((field) => {
          expect(fakeResponse[field]).equal(response[0][field]);
        });
        expect(db.all.calledOnce).to.be.true;
      });
    });
    describe("find from database with sql error threw", () => {
      it("should invoke db all function to throw an error", async () => {
        const db: any = {
          all: sinon.stub().callsArgWith(2, new Error("sql error"), null),
        };
        const dataSource = new DatabaseSource<Ride>(db, "Rides");
        expect(dataSource).not.to.undefined;

        try {
          await dataSource.find();
        } catch (e) {
          expect(e).to.be.not.undefined;
        } finally {
          expect(db.all.calledOnce).to.be.true;
        }
      });
    });
  });

  describe("#getById", () => {
    describe("getById success", () => {
      it("shoud invoke db all function to return data", async () => {
        const fakeResponse = {
          rideID: 1,
          startLat: 2,
          startLong: 3,
          endLat: 4,
          endLong: 9,
          riderName: "Hieu",
          driverName: "Hieu",
          driverVehicle: "SH",
          created: "2021-08-21 11:36:54",
        };
        const db: any = {
          all: sinon.stub().callsArgWith(2, null, [fakeResponse]),
        };
        const dataSource = new DatabaseSource<Ride>(db, "Rides");
        expect(dataSource).not.to.undefined;

        const response = await dataSource.getById(1);
        Object.keys(fakeResponse).forEach((field) => {
          expect(fakeResponse[field]).equal(response[field]);
        });
        expect(db.all.calledOnce).to.be.true;
      });
    });

    describe("getById from database with but no result", () => {
      it("should invoke db all function to throw a not found error", async () => {
        const db: any = {
          all: sinon.stub().callsArgWith(2, null, []),
        };
        const dataSource = new DatabaseSource<Ride>(db, "Rides");
        expect(dataSource).not.to.undefined;

        try {
          await dataSource.getById(1);
        } catch (e) {
          expect(e.message).equal("Could not find any ride");
          expect(e).to.be.not.undefined;
        } finally {
          expect(db.all.calledOnce).to.be.true;
        }
      });
    });

    describe("getById from database with error threw", () => {
      it("should invoke db all function to throw an error", async () => {
        const db: any = {
          all: sinon.stub().callsArgWith(2, new Error("sql error"), null),
        };
        const dataSource = new DatabaseSource<Ride>(db, "Rides");
        expect(dataSource).not.to.undefined;

        try {
          await dataSource.getById(1);
        } catch (e) {
          expect(e).to.be.not.undefined;
        } finally {
          expect(db.all.calledOnce).to.be.true;
        }
      });
    });
  });

  describe("#create", () => {
    describe("create ride data successfully", () => {
      it("should invoke db run function to resolve rideId", async () => {
        const db: any = {
          run: sinon.stub().callsArgWith(2, null, 1),
        };
        const dataSource = new DatabaseSource<Ride>(db, "Rides");
        expect(dataSource).not.to.undefined;

        const fakeData = {
          startLat: 2,
          startLong: 3,
          endLat: 4,
          endLong: 9,
          riderName: "Nhat",
          driverName: "Hieu",
          driverVehicle: "Honda SH",
        };

        const rideId = await dataSource.create(fakeData);
        expect(rideId).equal(1);
        expect(db.run.calledOnce).to.be.true;
      });
    });

    describe("insert to database with error threw", () => {
      it("should invoke db run function to throw an error", async () => {
        const db: any = {
          run: sinon.stub().callsArgWith(2, new Error("sql error"), null),
        };
        const dataSource = new DatabaseSource<Ride>(db, "Rides");
        expect(dataSource).not.to.undefined;

        const fakeData = {
          startLat: 2,
          startLong: 3,
          endLat: 4,
          endLong: 9,
          riderName: "Nhat",
          driverName: "Hieu",
          driverVehicle: "Honda SH",
        };

        try {
          await dataSource.create(fakeData);
        } catch (e) {
          expect(e).to.be.not.undefined;
        } finally {
          expect(db.run.calledOnce).to.be.true;
        }
      });
    });
  });
});
