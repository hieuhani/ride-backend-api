import sinon from "sinon";
import { Database } from "sqlite3";
import { Ride } from "../../src/ride/models";
import { DatabaseSource } from "../../src/db";
import { RideRepository } from "../../src/ride";
import { expect } from "chai";

describe("RideRepository", () => {
  describe("constructor", () => {
    it("should be initialized", (done) => {
      const db = new Database(":memory:");
      const rideDataSource = new DatabaseSource<Ride>(db, "Rides");
      const repo = new RideRepository(rideDataSource);
      expect(repo).not.to.undefined;
      done();
    });
  });

  describe("#create", () => {
    it("should invoke datasource create function", (done) => {
      const rideDataSource: any = {
        create: sinon.spy(),
      };
      const repo = new RideRepository(rideDataSource);
      expect(repo).not.to.undefined;
      const fakeData = {
        startLat: 2,
        startLong: 3,
        endLat: 4,
        endLong: 9,
        riderName: "Nhat",
        driverName: "Hieu",
        driverVehicle: "Honda SH",
      };
      repo.create(fakeData);

      expect(rideDataSource.create.calledOnce).to.be.true;
      expect(rideDataSource.create.calledWith(fakeData)).to.be.true;
      done();
    });
  });

  describe("#getById", () => {
    it("should invoke datasource getById function", (done) => {
      const rideDataSource: any = {
        getById: sinon.spy(),
      };
      const repo = new RideRepository(rideDataSource);
      expect(repo).not.to.undefined;
      const rideId = 6;
      repo.getById(rideId);

      expect(rideDataSource.getById.calledOnce).to.be.true;
      expect(rideDataSource.getById.calledWith(rideId)).to.be.true;
      done();
    });
  });

  describe("#find", () => {
    it("should invoke datasource find function", (done) => {
      const rideDataSource: any = {
        find: sinon.spy(),
      };
      const repo = new RideRepository(rideDataSource);
      expect(repo).not.to.undefined;
      repo.find(10, 0);

      expect(rideDataSource.find.calledOnce).to.be.true;
      expect(rideDataSource.find.calledWith(10, 0)).to.be.true;
      done();
    });
  });
});
