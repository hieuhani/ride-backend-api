import sinon from "sinon";
import { expect } from "chai";
import httpMocks from "node-mocks-http";
import proxyquire from "proxyquire";
import { Database } from "sqlite3";

import {
  checkValidationResult,
  injectDIContainer,
} from "../../src/libs/middleware";

describe("test checkValidationResult middleware", () => {
  it("should response data with a corresponding status code", (done) => {
    const spyNext = sinon.spy(() => {
      expect(spyNext.calledOnce).to.be.true;
      done();
    });
    const req = httpMocks.createRequest();
    const res = httpMocks.createResponse();
    checkValidationResult(req, res, spyNext);
  });

  it("should response errors with a status code 422", (done) => {
    const stubValidationResult = sinon.stub();
    stubValidationResult.returns({
      isEmpty: () => false,
      array: () => [],
    });

    const spyNext = sinon.spy();
    const req = httpMocks.createRequest();
    const middleware = proxyquire("../../src/libs/middleware", {
      "express-validator": {
        validationResult: stubValidationResult,
      },
    });
    const res: any = {
      json: sinon.spy(),
      status: sinon.stub().returns({ json: sinon.spy() }),
    };
    middleware.checkValidationResult(req, res, spyNext);
    expect(res.status.calledWith(422)).to.be.true;
    done();
  });
});

describe("test injectDIContainer", () => {
  it("should be inject the di container to the request object", (done) => {
    const spyNext = sinon.spy(() => {
      expect(spyNext.calledOnce).to.be.true;
    });
    const req = httpMocks.createRequest();
    const res = httpMocks.createResponse();
    injectDIContainer({ db: new Database(":memory:") })(req, res, spyNext);
    expect(req.di).not.to.undefined;
    done();
  });
});
