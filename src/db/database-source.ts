import { Database } from "sqlite3";
import { DataSourceAdapter } from "../base";

export class DatabaseSource<T> implements DataSourceAdapter<T> {
  private readonly table: string;
  private readonly db: Database;

  constructor(db: Database, table: string) {
    this.db = db;
    this.table = table;
  }

  find(limit = 10, offset = 0): Promise<T[]> {
    return new Promise((resolve, reject) => {
      return this.db.all(
        `SELECT * FROM ${this.table} LIMIT ? OFFSET ?`,
        [limit, offset],
        function (err: Error | null, rows) {
          if (err) {
            return reject(err);
          }

          return resolve(rows);
        }
      );
    });
  }

  getById(id: number): Promise<T> {
    return new Promise((resolve, reject) => {
      return this.db.all(
        `SELECT * FROM ${this.table} WHERE rideID = ?`,
        [id],
        function (err: Error | null, rows) {
          if (err) {
            return reject(err);
          }
          if (!rows[0]) {
            return reject(new Error("Could not find any ride"));
          }
          return resolve(rows[0]);
        }
      );
    });
  }

  create(data: T): Promise<number> {
    const fields = Object.keys(data).join(", ");
    const placeholders = Object.keys(data)
      .map(() => "?")
      .join(", ");
    return new Promise((resolve, reject) => {
      return this.db.run(
        `INSERT INTO ${this.table}(${fields}) VALUES (${placeholders})`,
        Object.values(data),
        function (err: Error | null, dataTest) {
          // TODO: dataTest looks weird, but it's a possible way for stubbing data
          if (err) {
            return reject(err);
          }
          return resolve(this?.lastID || dataTest);
        }
      );
    });
  }
}
