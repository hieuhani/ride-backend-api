import { Database as DatabaseInterface, verbose } from "sqlite3";
import { promisify } from "util";

const DDL_SQL = `
    CREATE TABLE IF NOT EXISTS Rides(
        rideID INTEGER PRIMARY KEY AUTOINCREMENT,
        startLat DECIMAL NOT NULL,
        startLong DECIMAL NOT NULL,
        endLat DECIMAL NOT NULL,
        endLong DECIMAL NOT NULL,
        riderName TEXT NOT NULL,
        driverName TEXT NOT NULL,
        driverVehicle TEXT NOT NULL,
        created DATETIME default CURRENT_TIMESTAMP
    )
`;

const { Database } = verbose();

export const db = new Database(":memory:");
export const serializeDb = promisify(db.serialize.bind(db));
export const runSql = promisify(db.run.bind(db));

export const closeDb = promisify(db.close.bind(db));

export const initializeDb = async (): Promise<DatabaseInterface> => {
  await serializeDb();
  await runSql(DDL_SQL);
  return db;
};
