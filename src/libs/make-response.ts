import { Response } from "express";

export function makeResponse(
  res: Response,
  statusCode: number,
  data?: unknown,
  error?: Error
): void {
  res.status(statusCode).json({
    statusCode,
    data,
    error,
  });
}
