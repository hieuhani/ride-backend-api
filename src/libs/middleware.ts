import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import { AppDependency } from "../types";
import { DatabaseSource } from "../db";
import { RideRepository } from "../ride";
import { Ride } from "../ride/models";

export function checkValidationResult(
  req: Request,
  res: Response,
  next: NextFunction
): void {
  const result = validationResult(req);
  if (result.isEmpty()) {
    return next();
  }

  res.status(422).json({ errors: result.array() });
}

export function injectDIContainer({ db }: AppDependency) {
  return (req: Request, res: Response, next: NextFunction): void => {
    const rideDataSource = new DatabaseSource<Ride>(db, "Rides");
    req.di = {
      repositories: {
        ride: new RideRepository(rideDataSource),
      },
    };
    return next();
  };
}
