import { Request, Response } from "express";
import { makeResponse } from "../libs/make-response";
import { Ride } from "./models";

export const getRides = async (req: Request, res: Response): Promise<void> => {
  try {
    const rides = await req.di.repositories.ride.find(
      Number.isInteger(req.query.limit) ? Number(req.query.limit) : undefined,
      Number.isInteger(req.query.offset) ? Number(req.query.offset) : undefined
    );
    return makeResponse(res, 200, rides);
  } catch (err) {
    return makeResponse(res, 500, null, err);
  }
};

export const getRideById = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    const ride = await req.di.repositories.ride.getById(Number(req.params.id));
    return makeResponse(res, 200, ride);
  } catch (err) {
    return makeResponse(res, 500, null, err);
  }
};

export const postCreateRide = async (
  req: Request,
  res: Response
): Promise<void> => {
  const ride: Ride = {
    startLat: req.body.start_lat,
    startLong: req.body.start_long,
    endLat: req.body.end_lat,
    endLong: req.body.end_long,
    riderName: req.body.rider_name,
    driverName: req.body.driver_name,
    driverVehicle: req.body.driver_vehicle,
  };

  try {
    const newRideId = await req.di.repositories.ride.create(ride);
    return makeResponse(res, 200, {
      ...ride,
      rideID: newRideId,
    });
  } catch (err) {
    return makeResponse(res, 500, null, err);
  }
};
