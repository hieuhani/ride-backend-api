import { BaseRepository } from "../base";
import { Ride } from "./models";

export class RideRepository extends BaseRepository<Ride> {}
