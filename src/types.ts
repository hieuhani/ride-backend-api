import { Database } from "sqlite3";
import { RideRepository } from "./ride";

export type Undefinable<T> = T | undefined;

export interface AppDependency {
  db: Database;
}

export interface RequestDI {
  repositories: {
    ride: RideRepository;
  };
}

declare module "express" {
  interface Request {
    di: RequestDI;
  }
}
